package com.alankin.patrones.abstractFactory;


import com.alankin.patrones.entity.Note;
import com.alankin.patrones.entity.User;
import com.alankin.patrones.response.NoteResponse;

public abstract class NoteResponseFactory {
    public abstract NoteResponse create(Note note, User user);
}
