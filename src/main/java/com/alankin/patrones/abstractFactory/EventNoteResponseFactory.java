package com.alankin.patrones.abstractFactory;

import com.alankin.patrones.entity.Note;
import com.alankin.patrones.entity.User;
import com.alankin.patrones.response.NoteResponse;

public class EventNoteResponseFactory extends NoteResponseFactory {
    @Override
    public NoteResponse create(Note note, User user) {
        return new NoteResponse.Builder()
                .setId(note.getId())
                .setDate(note.getDate())
                .setNoteType(note.getNoteType())
                .setTitle(note.getTitle())

                .setUsername(user.getName())
                .setLastName(user.getLastName())

                .build();
    }
}
