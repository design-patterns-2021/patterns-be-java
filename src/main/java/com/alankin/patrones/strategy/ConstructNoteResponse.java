package com.alankin.patrones.strategy;

import com.alankin.patrones.abstractFactory.NoteResponseFactory;
import com.alankin.patrones.entity.Note;
import com.alankin.patrones.entity.User;
import com.alankin.patrones.response.NoteResponse;

public interface ConstructNoteResponse {

    public NoteResponse buildNote(NoteResponseFactory factory, Note note, User user);
}
