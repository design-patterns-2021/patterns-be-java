package com.alankin.patrones.strategy;

import com.alankin.patrones.abstractFactory.NoteResponseFactory;
import com.alankin.patrones.entity.Note;
import com.alankin.patrones.entity.User;
import com.alankin.patrones.response.NoteResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class NoteResponseManager {

    private ConstructNoteResponse constructNoteResponse;

    public void setConstructNoteResponseStrategy(ConstructNoteResponse constructNoteResponseStrategy) {
        this.constructNoteResponse = constructNoteResponseStrategy;
    }

    public NoteResponse construct(NoteResponseFactory factory, Note note, User user) {
        return this.constructNoteResponse.buildNote(factory, note, user);
    }

}