package com.alankin.patrones.entity;

import com.alankin.patrones.enums.NoteType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "note")
public class Note {

    @Id
    private String id;
    private String title;
    private Date date;
    private NoteType noteType;
    private User user;
}
