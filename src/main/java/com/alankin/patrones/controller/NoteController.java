package com.alankin.patrones.controller;

import com.alankin.patrones.abstractFactory.EventNoteResponseFactory;
import com.alankin.patrones.abstractFactory.TodoNoteResponseFactory;
import com.alankin.patrones.entity.Note;
import com.alankin.patrones.enums.NoteType;
import com.alankin.patrones.response.NoteResponse;
import com.alankin.patrones.service.INoteService;
import com.alankin.patrones.service.UserService;
import com.alankin.patrones.strategy.ConstructNoteResponse;
import com.alankin.patrones.strategy.ConstructNoteResponseEventStrategy;
import com.alankin.patrones.strategy.ConstructNoteResponseTodoStrategy;
import com.alankin.patrones.strategy.NoteResponseManager;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(
        tags = "notes-controller"
)
@RequestMapping(value = "/notes")
@RestController
public class NoteController {

    private INoteService transaction;

    private NoteResponseManager noteResponseManager;

    public NoteController(INoteService transaction) {
        this.transaction = transaction;
        this.noteResponseManager = new NoteResponseManager();
    }

    @GetMapping()
    public List<NoteResponse> load() {
        List<NoteResponse> response = new ArrayList<NoteResponse>();

        ConstructNoteResponse eventStrategy = new ConstructNoteResponseEventStrategy();
        ConstructNoteResponse todoStrategy = new ConstructNoteResponseTodoStrategy();


        List<Note> notes = (List<Note>) transaction.findAll();
        for (Note note : notes) {

            if (note.getNoteType().equals(NoteType.EVENT)) {
                noteResponseManager.setConstructNoteResponseStrategy(eventStrategy);
                response.add(noteResponseManager.construct(new EventNoteResponseFactory(), note, UserService.getInstance().getLoggedInUser()));
            }

            if (note.getNoteType().equals(NoteType.TODO)) {
                noteResponseManager.setConstructNoteResponseStrategy(todoStrategy);
                response.add(noteResponseManager.construct(new TodoNoteResponseFactory(), note, note.getUser()));
            }
        }
        return response;
    }

    @PostMapping()
    public Note save(@RequestBody Note note) {
        return transaction.save(note);
    }
}

