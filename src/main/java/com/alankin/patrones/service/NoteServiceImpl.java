package com.alankin.patrones.service;

import com.alankin.patrones.entity.Note;
import com.alankin.patrones.repository.INoteRepository;
import org.springframework.stereotype.Service;

@Service
public class NoteServiceImpl implements INoteService {

    private INoteRepository repository;

    public NoteServiceImpl(INoteRepository repository) {
        this.repository = repository;
    }

    @Override
    public Note save(Note note) {
        return repository.save(note);
    }

    @Override
    public Iterable<Note> findAll() {
        return repository.findAll();
    }
}
