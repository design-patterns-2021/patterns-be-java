package com.alankin.patrones.service;

import com.alankin.patrones.entity.User;

public final class UserService {
    private static UserService instance;

    private UserService() {
    }

    public static UserService getInstance() {
        if (instance == null) {
            instance = new UserService();
        }
        return instance;
    }

    public User getLoggedInUser() {
        return new User("123ABC", "Alain", "Quinones");
    }

    public User getUser() {
        return new User("456DEF", "Jhon", "Doe");
    }
}