package com.alankin.patrones.service;

import com.alankin.patrones.entity.Note;

public interface INoteService {

    public Note save(Note note);

    public Iterable<Note> findAll();
}