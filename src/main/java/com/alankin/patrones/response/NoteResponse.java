package com.alankin.patrones.response;

import com.alankin.patrones.enums.NoteType;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class NoteResponse {

    private String id;
    private String title;
    private Date date;
    private NoteType noteType;
    private String username;
    private String lastName;

    public static class Builder {
        private String id;
        private String title;
        private Date date;
        private NoteType noteType;
        private String username;
        private String lastName;

        public Builder() {
        }

        public NoteResponse.Builder setId(String id) {
            this.id = id;
            return this;
        }

        public NoteResponse.Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public NoteResponse.Builder setDate(Date date) {
            this.date = date;
            return this;
        }

        public NoteResponse.Builder setNoteType(NoteType noteType) {
            this.noteType = noteType;
            return this;
        }

        public NoteResponse.Builder setUsername(String username) {
            this.username = username;
            return this;
        }

        public NoteResponse.Builder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public NoteResponse build() {
            NoteResponse noteResponse = new NoteResponse();
            noteResponse.setId(this.id);
            noteResponse.setDate(this.date);
            noteResponse.setLastName(this.lastName);
            noteResponse.setNoteType(this.noteType);
            noteResponse.setTitle(this.title);
            noteResponse.setUsername(this.username);

            return noteResponse;
        }
    }
}
